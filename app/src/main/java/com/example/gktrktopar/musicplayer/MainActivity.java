package com.example.gktrktopar.musicplayer;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.app.SearchManager;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import static java.security.AccessController.getContext;

public class MainActivity extends AppCompatActivity implements MediaController.MediaPlayerControl {

    static ArrayList<Song> songList=new ArrayList<Song>();
    public static String MUSIC_NAME="music_name";
    public static String ARTIST="artist";
    static String SONG_ID="song_id";
    static String SONG_TIME="song_time";
    public static android.widget.SearchView searchView;
    public static  int selected_song_position;
    public static String searched_word;


    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent ıntent=getIntent();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
       // searchView= (SearchView) findViewById(R.id.search);
        searched_word="";


   /*     FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},1);
       ;
        //setController();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission granted and now can proceed
                    getSongList(); //a sample method called

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(MainActivity.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }
            // add other cases for more permissions
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Toast.makeText(getApplicationContext(),"Deneme",Toast.LENGTH_SHORT).show();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void start() {

    }

    @Override
    public void pause() {

    }

    @Override
    public int getDuration() {
        return 0;
    }

    @Override
    public int getCurrentPosition() {
        return 0;
    }

    @Override
    public void seekTo(int pos) {

    }

    @Override
    public boolean isPlaying() {
        return false;
    }

    @Override
    public int getBufferPercentage() {
        return 0;
    }

    @Override
    public boolean canPause() {
        return true;
    }

    @Override
    public boolean canSeekBackward() {
        return false;
    }

    @Override
    public boolean canSeekForward() {
        return false;
    }

    @Override
    public int getAudioSessionId() {
        return 0;
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        BooleanListener bv = new BooleanListener();
        TextView artist;
        TextView song_name;
        LinearLayout mediacontroller;
        ImageButton play;
        ImageButton pause;

        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            if (sectionNumber==1){

            }
            else if(sectionNumber==2){


            }
            else{

            }
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);

            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            final View rootView=inflater.inflate(R.layout.allmusics, container, false);
            artist=(TextView)rootView.findViewById(R.id.artist);
            song_name=(TextView)rootView.findViewById(R.id.music_name);
            mediacontroller=(LinearLayout)rootView.findViewById(R.id.media_controller);
            play=(ImageButton)mediacontroller.findViewById(R.id.start);
            pause=(ImageButton)mediacontroller.findViewById(R.id.pause);



            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
             artist.setText(preferences.getString("artist", ""));
            song_name.setText(preferences.getString("songname", ""));
            if (getArguments().getInt(ARG_SECTION_NUMBER)==1){
                LinearLayout mediacontroller=(LinearLayout) rootView.findViewById(R.id.media_controller);
                mediacontroller.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent ıntent=new Intent(getContext(),PlaySong.class);
                        ıntent.putExtra(SONG_ID,-10);
                        startActivity(ıntent);
                    }
                });
                allMusics(rootView,null);
               /* searchView.setOnQueryTextListener(new android.widget.SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        searched_word=query;
                        allMusics(rootView,searched_songs());
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        return false;
                    }
                });*/
            }
            else if(getArguments().getInt(ARG_SECTION_NUMBER)==2){

                son_eklenenler(rootView);

            }
            else{

            }

            /*
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));*/
            return rootView;
        }

        public void allMusics(View rootView, final ArrayList<Song> searchedsongs){

            if (searchedsongs==null){
                MusicsAdapter adapter;
            ListView list=(ListView)rootView.findViewById(R.id.all_musics);
            ArrayList<HashMap<String, String>> musics = new ArrayList<HashMap<String, String>>();
            for (int i = 0; i < songList.size(); i++) {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put(ARTIST,songList.get(i).getArtist_name());
                map.put(MUSIC_NAME,songList.get(i).getSong_name());

                musics.add(map);
            }

          adapter = new MusicsAdapter(this.getActivity(), musics);


            list.setAdapter(adapter);
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    selected_song_position=position;
                    artist.setText(songList.get(position).getArtist_name());
                    song_name.setText(songList.get(position).getSong_name());

                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
                    SharedPreferences.Editor editor = preferences.edit();

                    editor.putString("artist", songList.get(position).getArtist_name());
                    editor.putString("songname", songList.get(position).getSong_name());
                    editor.putInt("songposition",position);
                    editor.commit();
                    PlaySong.setList(songList);
                    Intent ıntent=new Intent(getContext(),PlaySong.class);

                    startActivity(ıntent);

                }
            });
            }
            else{
                MusicsAdapter adapter;
                ListView list=(ListView)rootView.findViewById(R.id.all_musics);
                ArrayList<HashMap<String, String>> musics = new ArrayList<HashMap<String, String>>();
                for (int i = 0; i < searchedsongs.size(); i++) {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put(ARTIST,searchedsongs.get(i).getArtist_name());
                    map.put(MUSIC_NAME,searchedsongs.get(i).getSong_name());

                    musics.add(map);
                }

                adapter = new MusicsAdapter(this.getActivity(), musics);


                list.setAdapter(adapter);
                list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        selected_song_position=position;
                        artist.setText(searchedsongs.get(position).getArtist_name());
                        song_name.setText(searchedsongs.get(position).getSong_name());

                        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
                        SharedPreferences.Editor editor = preferences.edit();

                        editor.putString("artist", searchedsongs.get(position).getArtist_name());
                        editor.putString("songname", searchedsongs.get(position).getSong_name());
                        editor.putInt("songposition",position);
                        editor.commit();
                        PlaySong.setList(searchedsongs);
                        Intent ıntent=new Intent(getContext(),PlaySong.class);

                        startActivity(ıntent);

                    }
                });

            }








        }
        public ArrayList<Song> searched_songs(){
            if (!searched_word.equals("")){
                ArrayList<Song> searchedSongList=new ArrayList<Song>();
            for (int i = 0; i <songList.size() ; i++) {
                if (songList.get(i).getArtist_name().contains(searched_word)||
                        songList.get(i).getSong_name().contains(searched_word)){
                  Song new_song=new Song();
                    new_song=songList.get(i);
                    searchedSongList.add(new_song);
                }
            }
                return  searchedSongList;
            }
            else{
                return  null;
            }
        }
        public void son_eklenenler(View rootView){


            MusicsAdapter adapter;
            ListView list=(ListView)rootView.findViewById(R.id.all_musics);
            ArrayList<HashMap<String, String>> musics = new ArrayList<HashMap<String, String>>();
            ArrayList<Song> son_eklenenler=new ArrayList<Song>();
            for (int i = songList.size()-1; i > songList.size()-10; i--) {

                son_eklenenler.add(songList.get(i));

            }
            for (int i =0; i <son_eklenenler.size(); i++) {

                HashMap<String, String> map = new HashMap<String, String>();
                map.put(ARTIST,son_eklenenler.get(i).getArtist_name());
                map.put(MUSIC_NAME,son_eklenenler.get(i).getSong_name());
                musics.add(map);

            }

            adapter = new MusicsAdapter(this.getActivity(), musics);
            list.setAdapter(adapter);
            PlaySong.setList(son_eklenenler);
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    selected_song_position=position;
                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
                    SharedPreferences.Editor editor = preferences.edit();

                    editor.putString("artist", songList.get(position).getArtist_name());
                    editor.putString("songname", songList.get(position).getSong_name());
                    editor.putInt("songposition",position);
                    editor.commit();
                    Intent ıntent=new Intent(getContext(),PlaySong.class);
                    ıntent.putExtra(SONG_ID,position);
                    startActivity(ıntent);
                }
            });



        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Parçalar";
                case 1:
                    return "Son Eklenenler";
                case 2:
                    return "SECTION 3";
            }
            return null;
        }
    }
    public void getSongList(){

        ContentResolver musicResolver = getContentResolver();
        Uri musicUri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Cursor musicCursor = musicResolver.query(musicUri, null, null, null, null);
        if(musicCursor!=null && musicCursor.moveToFirst()){
            //get columns
            int titleColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media.TITLE);
            int idColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media._ID);
            int artistColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media.ARTIST);
            int duration = musicCursor.getColumnIndex
                    (MediaStore.Audio.Media.DURATION);

            //add songs to list
            do {
                long thisId = musicCursor.getLong(idColumn);
                String thisTitle = musicCursor.getString(titleColumn);
                String thisArtist = musicCursor.getString(artistColumn);
                int thisduration=musicCursor.getInt(duration);
                Song song=new Song();
                song.setId(thisId);
                song.setDuration(thisduration);
                song.setArtist_name(thisArtist);
                song.setSong_name(thisTitle);
                long duration_control= TimeUnit.MILLISECONDS.toSeconds((long) thisduration);
                if(duration_control>45){
                    songList.add(song);
                }


            }
            while (musicCursor.moveToNext());
        }

    }
    public void PlaySongScreen(View v){
        Intent ıntent=new Intent(getApplicationContext(),PlaySong.class);
        ıntent.putExtra(SONG_ID,selected_song_position);
        startActivity(ıntent);

    }


}
