package com.example.gktrktopar.musicplayer;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.NotificationCompat;
import android.widget.MediaController;
import android.widget.SeekBar;

import java.util.ArrayList;

/**
 * Created by Göktürk Topar on 9.08.2017.
 */

public class MusicService extends Service implements
        MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener,
        MediaPlayer.OnCompletionListener {

    private final IBinder musicBind = new MusicBinder();
    //media player
    public MediaPlayer player;

    MediaController.MediaPlayerControl controller;
    //song list
    private ArrayList<Song> songs;
    //current position
    private int songPosn;
    private double startTime = 0;
    private double finalTime = 0;
    private Handler myHandler;
    public static int oneTimeOnly = 0;
    SeekBar seekBar;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return musicBind;
    }
    @Override
    public boolean onUnbind(Intent intent){
      /*  player.stop();
        player.release();*/
        return false;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {

    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mp.start();
    }

    public void initMusicPlayer(){
        player.setWakeMode(getApplicationContext(),
                PowerManager.PARTIAL_WAKE_LOCK);
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);
        player.setOnPreparedListener(this);
        player.setOnCompletionListener(this);
        player.setOnErrorListener(this);

    }
    public void setList(ArrayList<Song> songs){
        this.songs=songs;
    }
    public class MusicBinder extends Binder {
        MusicService getService() {
            return MusicService.this;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void playSong(int position){

            player.reset();
        setNotification("deneme");
            Song song = songs.get(songPosn);
            long song_id = song.getId();

            //  seekbarset();
            Uri trackUri = ContentUris.withAppendedId(
                    android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                    song_id);

            try {
                player.setDataSource(getApplicationContext(), trackUri);

            } catch (Exception i) {

            }

            player.prepareAsync();



    }
    public void setSong(int index){
        songPosn=index;
    }
    public Song getCurrentSong(){
        return songs.get(songPosn);
    }

    @Override
    public void onCreate(){
        super.onCreate();
        songPosn=0;
        player=new MediaPlayer();
        initMusicPlayer();


    }

    public void pause(){
        player.pause();
    }
    public void resume(){
        player.start();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void setNotification(String songname){
        NotificationCompat.Builder notification_builder=new NotificationCompat.Builder(getApplicationContext());
        notification_builder.setSmallIcon(R.drawable.play);
        notification_builder.setTicker(songname);
        notification_builder.setAutoCancel(true);
        notification_builder.setContentTitle(songname);
        notification_builder.setOngoing(true);
        Intent resultIntent = new Intent(getApplicationContext(), PlaySong.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
        stackBuilder.addParentStack(PlaySong.class);

// Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
        notification_builder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

// notificationID allows you to update the notification later on.
        mNotificationManager.notify(1, notification_builder.build());
      mNotificationManager.cancel(1);
    }

}
