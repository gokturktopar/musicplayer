package com.example.gktrktopar.musicplayer;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Göktürk Topar on 9.08.2017.
 */

public class MusicsAdapter extends BaseAdapter {
        private Activity activity;
        private ArrayList<HashMap<String, String>> data;
        private static LayoutInflater inflater=null;
        //public ImageLoader imageLoader;

        public MusicsAdapter(Activity a, ArrayList<HashMap<String, String>> d) {
            activity = a;
            data=d;
            inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }
        public int getCount() {
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View vi=convertView;
            if(convertView==null)
                vi = inflater.inflate(R.layout.list_rows, null);

            TextView music_name = (TextView)vi.findViewById(R.id.music_name); //
            TextView artist = (TextView)vi.findViewById(R.id.artist); //




            HashMap<String, String> music = new HashMap<String, String>();
            music = data.get(position);

            // Setting all values in listview
            music_name.setText(music.get(MainActivity.MUSIC_NAME));
            artist.setText(music.get(MainActivity.ARTIST));

            return vi;
        }
}
