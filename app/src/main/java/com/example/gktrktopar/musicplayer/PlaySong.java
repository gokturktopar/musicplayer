package com.example.gktrktopar.musicplayer;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;


public class PlaySong extends AppCompatActivity {
     private static MusicService musicSrv;
    private Intent playIntent;
    private boolean musicBound=false;
    int song_position;
    TextView song_name;
    TextView artist;
    Boolean loop;
   static ImageButton pause;
    static ImageButton start;
    static double startTime = 0;
    static double finalTime = 0;
    static Handler myHandler;
    public static int oneTimeOnly = 0;
    private static ArrayList<Song> songs;
    static SeekBar seekbar;
    ImageButton loop_close;
    ImageButton loop_open;
    static TextView startTimeField;
    static TextView endTimeField;
    Boolean changedPosition;
    int song_time_position;
    int song_change_time;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_song);
        Intent intent=getIntent();
        loop_close=(ImageButton)findViewById(R.id.loop_close);
        loop_open=(ImageButton)findViewById(R.id.loop_open);


  song_name=(TextView)findViewById(R.id.song_name);
        artist=(TextView)findViewById(R.id.artist);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        song_position=preferences.getInt("songposition",-10);
        if (song_position!=-10) {
            song_name.setText(songs.get(song_position).getSong_name());
            artist.setText(songs.get(song_position).getArtist_name());
        }
        start=(ImageButton)findViewById(R.id.start);
        pause=(ImageButton)findViewById(R.id.pause);
        myHandler = new Handler();
        seekbar=(SeekBar)findViewById(R.id.seekBar1);
        startTimeField =(TextView)findViewById(R.id.sstart);
        endTimeField =(TextView)findViewById(R.id.sfin);
        changedPosition=false;
        loop=false;
        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar,  int progress, boolean fromUser) {

                if (fromUser){
                    musicSrv.player.seekTo(progress);
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


    }
    @Override
    protected void onPause() {
        super.onPause();
    }
    @Override
    protected void onStop() {
        super.onStop();
    }

    //connect to the service
    private ServiceConnection musicConnection = new ServiceConnection(){

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MusicService.MusicBinder binder = (MusicService.MusicBinder)service;
            //get service
            musicSrv = binder.getService();

            //pass list
            musicSrv.setList(songs);
            musicBound = true;
            if (song_position!=-10) {
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                SharedPreferences.Editor editor=preferences.edit();
               if (musicSrv.player.isPlaying()){
                   editor.putInt("songtime",musicSrv.player.getCurrentPosition());
               }
                musicSrv.setSong(song_position);
                musicSrv.playSong(0);



            }
            song_ending_controller();
            mediacontroller();

        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            musicBound = false;
        }
    };
    public void openloop(View v){

        loop_close.setVisibility(View.VISIBLE);
        loop_close.setClickable(true);
        loop_open.setVisibility(View.INVISIBLE);
        loop_open.setClickable(false);
        loop=false;
    }
    public void closeloop(View v){
        loop_close.setVisibility(View.INVISIBLE);
        loop_open.setVisibility(View.VISIBLE);
        loop_close.setClickable(false);
        loop_open.setClickable(true);
        loop=true;
    }
    @Override
    protected void onStart() {
        super.onStart();
        if(playIntent==null){
            playIntent = new Intent(this, MusicService.class);
            bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE);
            startService(playIntent);

        }


    }
    static void setList(ArrayList<Song> songslist){
        songs=songslist;
    }
    static boolean isplayingmusic(){
        if (musicSrv!=null) {
            if (musicSrv.player.isPlaying()) {
                return true;
            } else {
                return false;
            }
        }
        else{
            return false;
        }
    }
    public void start(View v){
        musicSrv.resume();
        start.setVisibility(View.INVISIBLE);
        pause.setVisibility(View.VISIBLE);

        mediacontroller();
    }
    public void pause(View v){
        musicSrv.pause();
        pause.setVisibility(View.INVISIBLE);
        start.setVisibility(View.VISIBLE);

        mediacontroller();
    }
    public static void start(){
        musicSrv.resume();
        start.setVisibility(View.INVISIBLE);
        pause.setVisibility(View.VISIBLE);

        mediacontroller();
    }
    public static void pause(){
        musicSrv.pause();
        pause.setVisibility(View.INVISIBLE);
        start.setVisibility(View.VISIBLE);

        mediacontroller();
    }
    public void back(View v){
        Intent intent=new Intent(getApplicationContext(),MainActivity.class);
        startActivity(intent);
    }
    public void play_next(View v){
        if (song_position+1<songs.size()){
            song_position++;

            musicSrv.setSong(song_position);
            musicSrv.playSong(0);
            song_name.setText(songs.get(song_position).getSong_name());
            artist.setText(songs.get(song_position).getArtist_name());
            mediacontroller();
        }
    }
    public void play_prev(View v){
        if (song_position-1>0){
            song_position--;
            musicSrv.setSong(song_position);
            musicSrv.playSong(0);
            song_name.setText(songs.get(song_position).getSong_name());
            artist.setText(songs.get(song_position).getArtist_name());
            mediacontroller();
        }
    }


    public void song_ending_controller(){
        musicSrv.player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {

        if (loop){
            musicSrv.playSong(0);
        }
                else {
            if (song_position + 1 < songs.size()) {
                song_position++;
                musicSrv.setSong(song_position);
                musicSrv.playSong(0);
                song_name.setText(songs.get(song_position).getSong_name());
                artist.setText(songs.get(song_position).getArtist_name());
            } else {
                song_position = 1;
                musicSrv.setSong(song_position);
                musicSrv.playSong(0);
                song_name.setText(songs.get(song_position).getSong_name());
                artist.setText(songs.get(song_position).getArtist_name());
            }
        }
            }
        });
    }
    public static void mediacontroller(){
        finalTime = musicSrv.getCurrentSong().getDuration();
        startTime = musicSrv.player.getCurrentPosition();
      //  if(oneTimeOnly == 0){
            seekbar.setMax(musicSrv.getCurrentSong().getDuration());
            oneTimeOnly = 1;
        //}
        //Muziğin toplamda ne kadar süre oldugunu  endTimeField controller ına yazdırıyoruz...
        long end_minute=TimeUnit.MILLISECONDS.toMinutes((long) finalTime);
        long end_second=TimeUnit.MILLISECONDS.toSeconds((long) finalTime) -
                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
                        toMinutes((long) finalTime));
        if (end_minute<10&&end_second<10){
            endTimeField.setText(String.format("0%d:0%d",end_minute
                    ,end_second
                    )
            );
        }
        else if (end_minute>=10&&end_second<10){
            endTimeField.setText(String.format("%d:0%d",end_minute
                    ,end_second
                    )
            );
        }
        else if (end_minute<10&&end_second>=10){
            endTimeField.setText(String.format("0%d:%d",end_minute
                    ,end_second
                    )
            );
        }
        else{
            endTimeField.setText(String.format("%d:%d",end_minute
                    ,end_second
                    )
            );
        }

        //Muziğin başladıgı andan itibaren gecen süreyi ,startTimeField controller ına yazdırıyoruz...

        long start_minute=TimeUnit.MILLISECONDS.toMinutes((long) startTime);
        long start_second=TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
                        toMinutes((long) startTime));
        if (start_minute<10&&start_second<10){
            startTimeField.setText(String.format("0%d:0%d",start_minute
                    ,start_second
                    )
            );
        }
        else  if (start_minute>=10&&start_second<10){
            startTimeField.setText(String.format("%d:0%d",start_minute
                    ,start_second
                    )
            );
        }
        else  if (start_minute<10&&start_second>=10){
            startTimeField.setText(String.format("0%d:%d",start_minute
                    ,start_second
                    )
            );
        }
        else{
            startTimeField.setText(String.format("%d:%d",start_minute
                    ,start_second
                    )
            );
        }

        seekbar.setProgress((int)startTime);
        myHandler.postDelayed(UpdateSongTime,100);


    }
    static Runnable UpdateSongTime = new Runnable() {
        public void run() {
            startTime = musicSrv.player.getCurrentPosition();
            long start_minute=TimeUnit.MILLISECONDS.toMinutes((long) startTime);
            long start_second=TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
                            toMinutes((long) startTime));
            if (start_minute<10&&start_second<10){
                startTimeField.setText(String.format("0%d:0%d",start_minute
                        ,start_second
                        )
                );
            }
           else if (start_minute>=10&&start_second<10){
                startTimeField.setText(String.format("%d:0%d",start_minute
                        ,start_second
                        )
                );
            }
            else  if (start_minute<10&&start_second>=10){
                startTimeField.setText(String.format("0%d:%d",start_minute
                        ,start_second
                        )
                );
            }
            else{
                startTimeField.setText(String.format("%d:%d",start_minute
                        ,start_second
                        )
                );
            }



            seekbar.setProgress((int)startTime);
            myHandler.postDelayed(this, 100);
        }
    };
}
